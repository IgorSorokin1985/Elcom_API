from django.shortcuts import render
from companies.models import Company
from rest_framework import viewsets, generics, status
from companies.serializer import CompanySerializer

# Create your views here.


class CompanyCreateAPIView(generics.CreateAPIView):
    serializer_class = CompanySerializer

    def perform_create(self, serializer):
        new_company = serializer.save()
        user = self.request.user
        user.company = new_company
        user.save()
        new_company.save()



class CompanyListAPIView(generics.ListAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()


class CompanyUpdateAPIView(generics.UpdateAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()


class CompanyDestroyAPIView(generics.DestroyAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()


class CompanyRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
