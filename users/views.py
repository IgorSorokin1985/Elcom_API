from rest_framework import generics
from users.serializers import UserSerializer, LogSerializer
from users.models import User, Log
from orders.models import Order
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenViewBase
# Create your views here.


class UserCreateAPIView(generics.CreateAPIView):
    """Creating new user"""
    serializer_class = UserSerializer

    def perform_create(self, serializer):
        new_user = serializer.save()
        password = serializer.data["password"]
        new_user.set_password(password)
        new_user.save()
        data = {
            "user": new_user,
        }
        new_order = Order.objects.create(**data)
        new_order.save()


class UserUpdateAPIView(generics.UpdateAPIView):
    """Updating user"""
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        new_user = serializer.save()
        password = serializer.data["password"]
        new_user.set_password(password)
        new_user.save()


class UserDestroyAPIView(generics.DestroyAPIView):
    """Deleting user"""
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated]


class UserRetrieveAPIView(generics.RetrieveAPIView):
    """Viewing user"""
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticated]


class LogCreateAPIView(generics.CreateAPIView):
    serializer_class = LogSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        new_log = serializer.save()
        new_log.user = self.request.user
        new_log.save()


class LogListAPIView(generics.ListAPIView):
    serializer_class = LogSerializer
    queryset = Log.objects.all()
    permission_classes = [IsAuthenticated]


class LogRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = LogSerializer
    queryset = Log.objects.all()
    permission_classes = [IsAuthenticated]
