from rest_framework import serializers
from users.models import User, Log
from orders.models import Order


class UserSerializer(serializers.ModelSerializer):
    """Serializer for User Model"""
    #last_order_id = serializers.SerializerMethodField()


    class Meta:
        model = User
        fields = "__all__"

    #def get_last_order_id(self, instance):
    #    order = Order.objects.filter(user=instance.pk).last()
    #    return order.pk


class LogSerializer(serializers.ModelSerializer):
    """Serializer for User Model"""
    class Meta:
        model = Log
        fields = "__all__"
