from django.core.management import BaseCommand

from users.models import User


class Command(BaseCommand):
    """
    Create Elcomtest user
    """

    def handle(self, *args, **options):
        user = User.objects.create(
            email='elcomtest1@test.com',
            name='elcomtest',
            password='12345',
            telegram_chat_id='1153056185',
            is_active=True,
        )

        user.set_password('admin')
        user.save()