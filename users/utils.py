from users.models import User


def get_user_by_telegram(request):
    telegram_chart_id = request.headers["Chartid"]
    try:
        return User.objects.get(telegram_chat_id=telegram_chart_id)
    except Exception:
        print('ERROR')
