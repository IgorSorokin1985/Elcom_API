from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from orders.models import Order, Position
from orders.serializer import OrderSerializer, PositionSerializer
from payments.models import Payment
from users.models import Log
from items.models import Item
from payments.utils import get_url_for_payment, create_invoice_pdf
from orders.utils import get_summa_order, send_email, create_message_for_email
from users.utils import get_user_by_telegram


'''Order'''


class OrderListAPIView(generics.ListAPIView):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        list_orders = super().get_queryset()
        return list_orders.filter(user=self.request.user).filter(status="R")


class OrderRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    permission_classes = [IsAuthenticated]


class OrderUpdateAPIView(generics.UpdateAPIView):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        updated_order = serializer.save()
        if updated_order.status == "R":
            data = {
                "user": updated_order.user,
            }
            new_order = Order.objects.create(**data)
            new_order.save()

            order = serializer.instance
            output_filename = create_invoice_pdf(order)
            if get_summa_order(order) < 999999:
                url_for_payment = get_url_for_payment(order)
            else:
                url_for_payment = False
            new_payment_data = {
                "order": order,
                "url_for_payment": url_for_payment,
                "invoice": output_filename
            }
            new_payment = Payment.objects.create(**new_payment_data)
            new_payment.save()
            message = create_message_for_email(order.user, url_for_payment, output_filename)
            send_email(f"Order {order} from {order.data}", message, order.user)
        new_log_data = {
            "user": self.request.user,
            "action": f"Changed status of order {updated_order.id} from {updated_order.data}."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        updated_order.save()


class GetLastOrderByUser(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, *args, **kwargs):
        user = self.request.user
        order = Order.objects.filter(user=user).last()
        if order:
            return Response({"order_id": order.pk,
                             "user_id": user.pk})
        else:
            data = {
                "user": user,
            }
            new_order = Order.objects.create(**data)
            new_order.save()
            return Response({"order_id": new_order.pk,
                             "user_id": user.pk})


'''Position'''


class PositionCreateAPIView(generics.CreateAPIView):
    serializer_class = PositionSerializer
    permission_classes = [IsAuthenticated]


class PositionListAPIView(generics.ListAPIView):
    serializer_class = PositionSerializer
    queryset = Position.objects.all()
    permission_classes = [IsAuthenticated]


class PositionRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = PositionSerializer
    queryset = Position.objects.all()
    permission_classes = [IsAuthenticated]


class PositionUpdateAPIView(generics.UpdateAPIView):
    serializer_class = PositionSerializer
    queryset = Position.objects.all()
    permission_classes = [IsAuthenticated]

    def perform_update(self, serializer):
        position = serializer.save()
        item = position.item
        new_log_data = {
            "user": self.request.user,
            "action": f"Changed quantity {item} on {position.quantity} ."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        position.save()


class PositionDestroyAPIView(generics.DestroyAPIView):
    serializer_class = PositionSerializer
    queryset = Position.objects.all()
    permission_classes = [IsAuthenticated]

    def perform_destroy(self, *args, **kwargs):
        position = self.get_object()
        item = position.item
        new_log_data = {
            "user": self.request.user,
            "action": f"Deleted {position.quantity} {item}."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        position.delete()


class PositionAddAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, *args, **kwargs):
        order_id = self.request.data["order"]
        item_id = self.request.data["item"]
        item = Item.objects.get(pk=item_id)
        quantity = int(self.request.data["quantity"])
        price = int(self.request.data["price"])
        positions = Position.objects.filter(order=order_id).filter(item=item_id).all()
        if len(positions) == 0:
            new_position_data = {
                "order_id": order_id,
                "item_id": item_id,
                "quantity": quantity,
                "price": price
            }
            new_position = Position.objects.create(**new_position_data)
            new_position.save()
        else:
            position = positions[0]
            position.quantity += quantity
            position.save()
        new_log_data = {
            "user": self.request.user,
            "action": f"Added {quantity}  {item}."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        return Response({"message": "ok"})


""" TELEGRAM VIEWS"""


class OrderListTelegramAPIView(generics.ListAPIView):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def get_queryset(self):
        user = get_user_by_telegram(self.request)
        list_orders = super().get_queryset()
        return list_orders.filter(user=user).filter(status="R")


class OrderRetrieveTelegramAPIView(generics.RetrieveAPIView):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()


class GetLastOrderByUserTelegram(APIView):

    def get(self, *args, **kwargs):
        user = get_user_by_telegram(self.request)
        order = Order.objects.filter(user=user).last()
        return Response({"order_id": order.pk,
                         "user_id": user.pk})


class PositionAddTelegramAPIView(APIView):

    def post(self, *args, **kwargs):
        order_id = self.request.data["order"]
        item_id = self.request.data["item"]
        item = Item.objects.get(pk=item_id)
        quantity = int(self.request.data["quantity"])
        price = int(self.request.data["price"])
        user = get_user_by_telegram(self.request)
        positions = Position.objects.filter(order=order_id).filter(item=item_id).all()
        if len(positions) == 0:
            new_position_data = {
                "order_id": order_id,
                "item_id": item_id,
                "quantity": quantity,
                "price": price
            }
            new_position = Position.objects.create(**new_position_data)
            new_position.save()
        else:
            position = positions[0]
            position.quantity += quantity
            position.save()
        new_log_data = {
            "user": user,
            "action": f"Added {quantity}  {item}."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        return Response({"message": "ok"})


class OrderUpdateTelegramAPIView(generics.UpdateAPIView):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    def perform_update(self, serializer):
        updated_order = serializer.save()
        if updated_order.status == "R":
            data = {
                "user": updated_order.user,
            }
            new_order = Order.objects.create(**data)
            new_order.save()

            order = serializer.instance
            output_filename = create_invoice_pdf(order)
            if get_summa_order(order) < 999999:
                url_for_payment = get_url_for_payment(order)
            else:
                url_for_payment = False
            new_payment_data = {
                "order": order,
                "url_for_payment": url_for_payment,
                "invoice": output_filename
            }
            new_payment = Payment.objects.create(**new_payment_data)
            new_payment.save()
            message = create_message_for_email(order.user, url_for_payment, output_filename)
            send_email(f"Order {order} from {order.data}", message, order.user)
        new_log_data = {
            "user": updated_order.user,
            "action": f"Changed status of order {updated_order.id} from {updated_order.data}."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        updated_order.save()


class PositionDestroyTelegramAPIView(generics.DestroyAPIView):
    serializer_class = PositionSerializer
    queryset = Position.objects.all()

    def perform_destroy(self, *args, **kwargs):
        position = self.get_object()
        item = position.item
        user = get_user_by_telegram(self.request)
        new_log_data = {
            "user": user,
            "action": f"Deleted {position.quantity} {item}."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        position.delete()
