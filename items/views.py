from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from items.models import Item, Category
from users.models import Log
from items.serializer import ItemSerializer, CategorySerializer
from users.permissions import IsTelegramUser
from users.models import User


class ItemsListAPIView(generics.ListAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()


class ItemsRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_object())
        user = self.request.user
        item = self.get_object()
        new_log_data = {
            "user": user,
            "action": f"Saw {item}."
        }
        new_log = Log.objects.create(**new_log_data)
        new_log.save()
        return Response(serializer.data)


class CategoriesListAPIView(generics.ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


"""TELEGRAM VIEW"""


class ItemsRetrieveTelegramAPIView(generics.RetrieveAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_object())
        telegram_chart_id = request.headers["Chartid"]
        try:
            user = User.objects.get(telegram_chat_id=telegram_chart_id)
            item = self.get_object()
            new_log_data = {
                "user": user,
                "action": f"Saw {item}."
            }
            new_log = Log.objects.create(**new_log_data)
            new_log.save()
        except Exception:
            print('ERROR')
        return Response(serializer.data)
